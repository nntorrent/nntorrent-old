/* nntorrent client which manipulates nntorrent daemon through
 * its socket
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h> // inet_pton
#include <getopt.h>

#include <base64.h>

typedef enum {
  COMMAND_QUIT,
  COMMAND_ADD,
  COMMAND_STATUS,
} command_type_t;

typedef struct {
  char *content;
  size_t size;
} response_t;

void
usage()
{
  fprintf(stderr,
          "Usage: nntc <command>\n\n"
          "Command:\n"
          "  --quit\t\t\tshuts down the daemon\n"
          "  -a, --add <torrent path>\tadds torrent to the daemon queue\n"
          "  -s, --status\t\t\tgets the list of torrents\n"
  );
}

void
initialize_socket(int *fd)
{
  sockaddr_in sock_addr;
  sock_addr.sin_family = AF_INET;
  sock_addr.sin_port  = (u_short) 8000;

  if (!inet_pton(AF_INET, "127.0.0.1", &sock_addr.sin_addr)) {
    perror("converting ip");
    exit(EXIT_FAILURE);
  }

  if ((*fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("socket creation");
    exit(EXIT_FAILURE);
  }

  if (connect(*fd, (sockaddr *) &sock_addr, (socklen_t) sizeof(sock_addr)) != 0) {
    perror("connect");
    exit(EXIT_FAILURE);
  }

  printf("Connected to server\n");
}

int
make_command(command_type_t cmd_type,
             char *arg,
             response_t *resp)
{
  switch(cmd_type) {
    case COMMAND_QUIT:
      {
        size_t result_size = strlen("quit");
        char *result = (char *) malloc(result_size + 1); // + \0
        strcpy(result, "quit");

        resp->size = result_size;
        resp->content = result;
      }

      break;

    case COMMAND_ADD:
      {
        size_t result_size = 0;
        size_t max_result_size = 1024;
        char *result = (char *) malloc(max_result_size);

        strcpy(result, "add ");
        result_size += 4;

        // the argument is a torrent file path
        FILE *f = fopen(arg, "rb");

        char buf[1024] = {0};
        ssize_t len;
        while ((len = fread(buf, sizeof(buf[0]), sizeof(buf), f)) > 0) {

          if (result_size + len >= max_result_size) {
            result = (char *) realloc(result, max_result_size*2);
            max_result_size *= 2;
          }

          // there are random null bytes in the middle of torrent files
          // not sure why, but str(n)cat wouldn't work
          memcpy(result+result_size, buf, len);
          result_size += len;
        }

        if (len < 0) {
          perror("error reading torrent file");
          return -1;
        }

        resp->content = base64_encode(result, result_size, &resp->size);
        free(result);

        fclose(f);
      }

      break;

    case COMMAND_STATUS:
      {
        size_t result_size = strlen("status");
        char *result = (char *) malloc(result_size + 1); // + \0
        strcpy(result, "status");

        resp->size = result_size;
        resp->content = result;
      }

      break;

    default:
      fprintf(stderr, "Something went horribly wrong. You shouldn't see this! :/\n");
      break;
  }

  return 0;
}

int send_command(int fd, char *cmd, size_t cmd_size) {
  // cmd + \r + \n + \0
  char *res = (char *) malloc(cmd_size + 3);
  memcpy(res, cmd, cmd_size);
  memcpy(res+cmd_size, "\r\n\0", 3);

  ssize_t len = send(fd, res, cmd_size + 3, 0);

  free(res);

  if (len <= 0) {
    if (len < 0) perror("sending command");
    return -1;
  }

  return 0;
}

char *
get_response(int fd)
{
  // returns a heap-allocated null-terminated string
  size_t res_size = 0;
  size_t max_res_size = 128;
  char *res = (char *) malloc(max_res_size);
  memset(res, 0, max_res_size);

  char buf[128] = {0};
  bool done = false;

  while (!done) {
    memset(buf, 0, sizeof(buf));
    ssize_t len = recv(fd, buf, sizeof(buf), 0);

    if (len <= 0) {
      if (len < 0) { free(res); return NULL; }
      break;
    }


    if (res_size + len >= max_res_size) {
      max_res_size *= 2;
      res = (char *) realloc(res, max_res_size);
    }

    // delimiter is \r\n, so strncat wouldn't work as it assumes \0 as
    // the end of the string. In practice, the \0 works well for delimiter
    // when getting response from the daemon
    memcpy(res+res_size, buf, len);

    if (res[res_size - 1] == '\0') {
      if (res[res_size - 2] == '\n' &&
          res[res_size - 3] == '\r') {
        done = true;
      }
    }
  }

  // chopping down the delimited before sending it for processing
  // in practice, \0 works well for delimiter here
  res[res_size - 3] = '\0';
  return res;
}

int
main(int argc, char* argv[])
{
  if (argc < 2) {
    usage();
    exit(EXIT_FAILURE);
  }

  int sock_fd;
  initialize_socket(&sock_fd);

  struct option options[] = {
    {"add",     required_argument,  0, 'a'},
    {"status",  no_argument,        0, 's'},
    {"quit",    no_argument,        0, 'q'},
    {0,         0,                  0, 0  },
  };

  int c = 0, opt_index = 0;

  response_t resp;
  resp.size = 0;
  resp.content = NULL;

  while (true) {
    c = getopt_long(argc, argv, "sa:", options, &opt_index);
    if (c == -1) break; // end of options

    switch (c) {
      case 'q':
        make_command(COMMAND_QUIT, NULL, &resp);
        send_command(sock_fd, resp.content, resp.size);

        break;

      case 'a':
        make_command(COMMAND_ADD, optarg, &resp);

        if (send_command(sock_fd, resp.content, resp.size) == 0) {
          char *response = get_response(sock_fd);
          if (response != NULL) {
            fprintf(stderr, response);
            fprintf(stderr, "\n");
            free(response);
          }
        }

        break;

      case 's':
        make_command(COMMAND_STATUS, NULL, &resp);

        if (send_command(sock_fd, resp.content, resp.size) == 0) {
          char *response = get_response(sock_fd);
          if (response != NULL) {
            fprintf(stderr, response);
            fprintf(stderr, "\n");
            free(response);
          }
        }

        break;

      default:
        usage();
        exit(EXIT_FAILURE);
    }
  }

  if (resp.content != NULL) {
    free(resp.content);
  }

  close(sock_fd);
  return 0;
}

