/* nntorrent daemon which does the actual work
 */

#include <iostream>
#include <utility>
#include <vector>

#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <getopt.h>
#include <signal.h>

#include <libtorrent/session.hpp>

#include <base64.h>

typedef struct {
  struct sockaddr     *sock_addr;
  int                  sock_fd;
  libtorrent::session *torrent_session;
  std::vector<libtorrent::torrent_handle> torrent_handles;
  bool quit;
} session_data_t;

typedef struct {
  u_short  port;
} opts_t;

typedef enum {
  COMMAND_QUIT,
  COMMAND_ADD,
  COMMAND_TORRENTS_STATUS,
} command_type_t;

typedef struct {
  command_type_t type;
  char           *argument;
  size_t         argument_size;
} command_t;

void signal_handler(int signum) {
  fprintf(stderr, "Terminating\n");
  exit(EXIT_FAILURE);
}

void
usage()
{
  printf("Usage: nntd [options]\n\n"
         "Options:\n"
         "  -h, --help\t\t\tshow this help message\n"
         "  --port <port>\t\t\tport to listen on.\n"
  );
}

int
make_command(char *cmd, size_t size, command_t *command)
{
  char *loc, *arg;

  if (cmd == NULL || strcmp(cmd, "") == 0) {
    return -1;
  }

  loc = strchr(cmd, ' ');
  // if loc is not null, it has an argument or a tail space character
  if (loc != NULL) {
    *loc = '\0';
    arg = loc+1;
  }

  if (strcmp(cmd, "quit") == 0 ||
      strcmp(cmd, "exit") == 0 ||
      strcmp(cmd, "getoffmysystem") == 0)
  {
    command->type = COMMAND_QUIT;
    return 0;
  }

  if (strcmp(cmd, "add") == 0) {
    command->type = COMMAND_ADD;

    if (arg == NULL)
      return -1;

    // -4 because of 'add' at the start
    command->argument = base64_decode(arg, size - 4, &command->argument_size);
    return 0;
  }

  if (strcmp(cmd, "status") == 0) {
    command->type = COMMAND_TORRENTS_STATUS;
    return 0;
  }

  return -1;
}

int
send_response(int fd, char *resp)
{
  // strlen(resp) + \r + \n + \0
  char *res = (char *) malloc(strlen(resp) + 3);
  memset(res, 0, strlen(resp) + 3);

  memcpy(res, resp, strlen(resp));
  memcpy(res+strlen(resp), "\r\n\0", 3);

  // ignore EPIPE
  ssize_t len = send(fd, res, strlen(res), MSG_NOSIGNAL);

  free(res);

  if (len <= 0) {
    if (len < 0) perror("sending response");
    return -1;
  }

  return 0;
}

char *
get_torrents_status(session_data_t *data) {
  std::string output;

  try {
    for (unsigned int i = 0; i < data->torrent_handles.size(); i++) {
      // LIBTORRENT VERSION 0.16
      output.append(data->torrent_handles[i].name());
      output.append("\n");
    }
  } catch (libtorrent::libtorrent_exception e) {
    fprintf(stderr, "An error occurred while processing this request: ");
    fprintf(stderr, e.what());
    fprintf(stderr, "\n");
  }

  char *res = (char *) malloc(strlen(output.c_str()) + 1);
  memset(res, 0, strlen(output.c_str() + 1));
  strcat(res, output.c_str());
  return res;
}

int
do_command(int fd, command_t *cmd, session_data_t *data)
{
  switch(cmd->type) {
    case COMMAND_QUIT:
      data->quit = true;
      break;

    case COMMAND_ADD:
      {
        libtorrent::error_code ec;
        libtorrent::add_torrent_params p;
        p.save_path = "./";

        p.ti = new libtorrent::torrent_info(cmd->argument,
                                            cmd->argument_size,
                                            ec);
        if (ec) return -1;

        libtorrent::torrent_handle h = data->torrent_session->add_torrent(p, ec);
        if (ec) return -1;

        data->torrent_handles.push_back(h);
      }

      break;

    case COMMAND_TORRENTS_STATUS:
      {
        char *resp = get_torrents_status(data);

        if (resp == NULL)
          return -1;

        if (send_response(fd, resp) != 0) {
          free(resp);
          return -1;
        }

        free(resp);
      }

      break;

    default:
      // unknown command
      return -1;
  }

  return 0;
}


int
initialize_opts(int argc, char **argv, opts_t *opts)
{
  /* help
   * port :: int :: default:8000
   */

  struct option options[] = {
    {"help",      no_argument,        0, 'h'},
    {"port",      required_argument,  0, 'p'},
    {0,           0,                  0, 0  },
  };

  int opt_index = 0;
  int c = 0;

  while (true) {
    c = getopt_long(argc, argv, "hp:", options, &opt_index);
    if (c == -1) break; // end of options

    switch (c) {
      case 'h':
        usage();
        exit(EXIT_SUCCESS);

      case 'p':
        {
          int a;
          sscanf(optarg, "%i", &a);
          opts->port = (u_short) a;
        }

        break;

      default:
        return -1;
    }
  }

  while (optind < argc) {
    fprintf(stderr, "Unknown option (ignored): %s\n", argv[optind++]);
  }

  return 0;
}

int
initialize_socket(session_data_t *data, opts_t *opts)
{
  int fd;

  sockaddr_in sock;
  sock.sin_family = AF_INET;
  sock.sin_port = opts->port;
  sock.sin_addr.s_addr = INADDR_ANY;

  // create the socket
  if ((fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    perror("ERROR: socket creation");
    return -1;
  }

  // bind it
  if (bind(fd, (sockaddr *) &sock, sizeof(sock)) != 0) {
    perror("ERROR: binding socket");
    return -1;
  }

  // start listening for connection
  if (listen(fd, 1) != 0) { // right now, only one connection is enough
    perror("ERROR: listening");
    return -1;
  }

  // returning valuable information
  data->sock_fd = fd;
  data->sock_addr = (sockaddr *) &sock;

  return 0;
}


int
main(int argc, char *argv[])
{
  signal(SIGINT, signal_handler);

  opts_t opts;
  memset(&opts, 0, sizeof(opts));

  session_data_t session_data;
  session_data.quit = false;
  session_data.torrent_session = new libtorrent::session();

  libtorrent::error_code ec;
  session_data.torrent_session->listen_on(std::make_pair<int, int>(40000, 40010), ec);
  if (ec) return -1;

  if (initialize_opts(argc, argv, &opts) != 0) {
    exit(EXIT_FAILURE);
  }

  if (initialize_socket(&session_data, &opts) != 0) {
    exit(EXIT_FAILURE);
  }

  // listen and accept, like a good kid :/
  while (true) {
    int peer_fd;
    sockaddr peer_addr;
    size_t t = sizeof(peer_addr);

    if ((peer_fd = accept(session_data.sock_fd, &peer_addr, (socklen_t *) &t)) < 0) {
      perror("ERROR: accepting connection");
      continue;
    }

    command_t cmd;
    cmd.argument = NULL;
    cmd.argument_size = 0;

    size_t peer_buf_size = 0;
    size_t max_peer_buf_size = 1024;
    char *peer_buf = (char *) malloc(max_peer_buf_size);
    memset(peer_buf, 0, max_peer_buf_size);

    char temp_buf[1024];
    bool done = false;

    // get all the input
    while (!done) {
      ssize_t len = recv(peer_fd, temp_buf, sizeof(temp_buf), 0);

      if (len <= 0) {
        if (len < 0) perror("recv error");
        done = true;
      }

      // something needs to be copied
      if (!done) {
        if (peer_buf_size + len >= max_peer_buf_size) {
          max_peer_buf_size *= 2;
          peer_buf = (char *) realloc(peer_buf, max_peer_buf_size);
        }

        // there are null bytes in torrent files, so str(n)cat doesn't work
        memcpy(peer_buf+peer_buf_size, temp_buf, len);
        peer_buf_size += len;

        /* checking for \r\n delimiter
         * torrent files seem to have 0xa (\n) and 0xd (\r) scattered
         * in them, but not \r\n, so it should work well as a delimiter
         *
         * The message should end with \0, so the "delimiter" is
         * actually \r\n\0. This could change in the future to allow two
         * commands to be send in one message with \r\n delimiter
         */
        if (peer_buf[peer_buf_size - 1] == '\0') {
          if (peer_buf[peer_buf_size - 2] == '\n' &&
              peer_buf[peer_buf_size - 3] == '\r') {
            done = true;
          }
        }
      }
    }

    // chopping down the delimiter
    char *a = strrchr(peer_buf, '\r');
    if (a != NULL) {
      *a = '\0';
      memset(peer_buf+peer_buf_size - 3, 0, 3);
      peer_buf_size -= 3; // including the \0

      if (make_command(peer_buf, peer_buf_size, &cmd) == 0) {
        if (do_command(peer_fd, &cmd, &session_data) == 0) {
          send_response(peer_fd, (char *) "success");
        }
      }
    } else {
      send_response(peer_fd, (char *) "ERROR: Misformatted command. Needs to end with \\r\\n");
      fprintf(stderr, "Received misformatted command\n");
    }

    free(peer_buf);
    if (cmd.argument != NULL) free(cmd.argument);

    memset(&cmd, 0, sizeof(cmd));
    close(peer_fd);

    if (session_data.quit) {
      break;
    }
  }

  // cleanup and quit time
  fflush(stdout);
  close(session_data.sock_fd);
  delete session_data.torrent_session;
  return 0;
}

