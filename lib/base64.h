#ifndef BASE64_H
#define BASE64_H

#include <stdlib.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

static const char encoding_table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char *base64_encode(const char *data, const size_t len, size_t *output_len);
char *base64_decode(const char *data, const size_t len, size_t *output_len);

#ifdef __cplusplus
}
#endif

#endif // BASE64_H
