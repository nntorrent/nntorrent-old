#include "base64.h"

#include <string.h>
#include <stdio.h>

int
get_decoding_index(char c) {
  if (c == '=')
    return 0;

  char *loc = strchr(encoding_table, c);
  if (loc == NULL)
    return -1;

  return loc - encoding_table;
}

char *
base64_encode(const char *data,
              const size_t len,
              size_t *output_len)
{
  char *input, *output;
  size_t input_len;
  /* for every 3 characters, there are 4 characters in base64
   *
   * 00000000 00000000 00000000
   * |----| >>2
   *       |-----| <<4 & >>4
   *              |-----| <<2 & >>6
   *                     |----| &
   */
  if (len % 3 == 0) {
    input_len = len;
    *output_len = len / 3 * 4;
  } else {
    // need to pad it with zeroes
    input_len = len + 3 - (len % 3);
    *output_len = (len / 3 + 1) * 4;
  }

  (*output_len)++; // leave room for \0

  input =  (char *) malloc(input_len);
  memset(input, 0, input_len);
  memcpy(input, data, len);

  output = (char *) malloc(*output_len);
  memset(output, 0, *output_len);

  for (uint8_t i = 0, o = 0;
       i <= input_len - 3, o <= *output_len - 4;
       i += 3, o += 4)
  {
    uint8_t first_set  =  input[i]   >> 2 & 0b00111111;

    uint8_t second_set = (input[i]   << 4 & 0b00110000)|
                         (input[i+1] >> 4 & 0b00001111);

    uint8_t third_set  = (input[i+1] << 2 & 0b00111100)|
                         (input[i+2] >> 6 & 0b00000011);

    uint8_t fourth_set =  input[i+2]      & 0b00111111;

    output[o] = encoding_table[first_set];
    output[o+1] = encoding_table[second_set];
    output[o+2] = encoding_table[third_set];
    output[o+3] = encoding_table[fourth_set];
  }

  /* padding
   * len % 3 can be 0, 1, or 2
   * if it's 1, only the first will pass, if it's 2, both will pass
   */
  if (len % 3 > 0)
    output[*output_len - 1] = '=';
  if (len % 3 > 1)
    output[*output_len - 2] = '=';

  free(input);

  output[*output_len] = '\0';
  return output;
}

char *
base64_decode(const char *data,
              const size_t len,
              size_t *output_len)
{
  /* assume len is a multiple of 4
   *
   * 000000 000000 000000 000000
   * |-------|
   *          |-------|
   *                   |-------|
   */

  if (len % 4 != 0) {
    fprintf(stderr, "ERROR: base64 string has improper length. It should be padded.\n");
    return NULL;
  }

  *output_len = len / 4 * 3;
  char *output = (char *) malloc(*output_len);

  for (int i = 0, o = 0;
       i <= len - 4, o <= *output_len - 3;
       i += 4, o += 3)
  {
    // index of character
    int offset1 = get_decoding_index(data[i]);
    int offset2 = get_decoding_index(data[i+1]);
    int offset3 = get_decoding_index(data[i+2]);
    int offset4 = get_decoding_index(data[i+3]);

    if (offset1 < 0 || offset2 < 0 || offset3 < 0 || offset4 < 0) {
      fprintf(stderr, "ERROR: decoding base64 string: unknown character\n");
      free(output);
      return NULL;
    }

    output[o]   = (offset1 << 2 & 0b11111100)|
                  (offset2 >> 4 & 0b00000011);
    output[o+1] = (offset2 << 4 & 0b11110000)|
                  (offset3 >> 2 & 0b00001111);
    output[o+2] = (offset3 << 6 & 0b11000000)|
                  (offset4      & 0b00111111);
  }

  // no ending \0 just to preserve binary format
  return output;
}

